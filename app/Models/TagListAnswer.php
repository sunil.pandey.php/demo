<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagListAnswer extends Model
{
    use HasFactory;
    public $table = "tagslistanswer";
    public $fillable = ['answer','user_id'];
}
