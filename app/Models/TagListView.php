<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagListView extends Model
{
    use HasFactory;
    public $table = "tagslistview";
    public $fillable = ['page_view'];
}
