<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TagList;
use App\Models\TagListAnswer;
use App\Models\TagListView;
use App\Models\User;
use Validator;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data=TagList::all();
        $formView=TagListView::orderBy('id','desc')->first();
        $dataView=TagListView::updateorCreate(['page_view'=>$formView['page_view']+1]);
        return view('home',compact('data'));
    }
    /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('adminHome');
    }
    public function addMorePost(Request $request)
    {
        $rules = [];


        foreach($request->input('name') as $key => $value) {
            $rules["name.{$key}"] = 'required';
        }


        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes()) {

            $data=TagList::all();
            if($data)
            TagList::truncate();
            foreach($request->input('name') as $key => $value) {
                TagList::create(['name'=>$value]);
            }


            return response()->json(['success'=>'done']);
        }


        return response()->json(['error'=>$validator->errors()->all()]);
    }
    public function addPost(Request $request)
    {
        $user_id=Auth::id();
        $rules = [];
        foreach($request->input('name') as $key => $value) {
            $rules["name.{$key}"] = 'required';
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $data=TagListAnswer::where('user_id',$user_id)->get();
            if(count($data))
            return response()->json(['duplicate'=>'Already submitted']);
            else
            {
            foreach($request->input('name') as $key => $value) {
                TagListAnswer::create([
                    'answer'=>$value,
                    'user_id'=>$user_id,
                ]);
            }
        }

            return response()->json(['success'=>'done']);
        }


        return response()->json(['error'=>$validator->errors()->all()]);
    }
    public function formData()
    {
        $formView=TagListView::orderBy('id','desc')->first();
        $pageView=$formView['page_view'];
        $submitCount = DB::table('tagslistanswer')
                 ->select('user_id', DB::raw('count(*) as total'))
                 ->groupBy('user_id')
                 ->get();
        $formSubmit=0;
        return view('adminFormData',compact('pageView','submitCount','formSubmit'));
    }
    public function userData()
    {
        $formTag=TagList::all();
        $formData=TagListAnswer::all();
        foreach($formData as $user)
            $userName[]=User::where('id',$user->user_id)->get();
        return view('adminUserData',compact('formTag','formData','userName'));
    }
}


