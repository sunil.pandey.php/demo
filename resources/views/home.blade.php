@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                <div class="form-group">
                            <form name="add_name" id="add_name">  


                                <div class="alert alert-danger print-error-msg" style="display:none">
                                <ul></ul>
                                </div>


                                <div class="alert alert-success print-success-msg" style="display:none">
                                <ul></ul>
                                </div>


                                <div class="table-responsive">  
                                    <table class="table table-bordered" id="dynamic_field"> 
                                            @foreach($data as $dt) 
                                            <tr> 
                                                <td><span>{{$dt->name}}</span></td>
                                                <td><input type="text" name="name[]" placeholder="Enter your Answer" class="form-control name_list" /></td>  
                                            </tr>   
                                                @endforeach
                                        
                                    </table>  
                                    <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
                                </div>


                            </form>  
                        </div> 
                        <script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "<?php echo url('user/addpost'); ?>"; 
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    console.log(data.duplicate);
                    if(data.error){
                        printErrorMsg(data.error);
                    }else if(data.duplicate){
                        i=1;
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Already submitted.</li>');
                    }
                    else{
                        i=1;
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                    }
                }  
           });  
      });  


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });  
</script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection