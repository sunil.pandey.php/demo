@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard<a href="{{route('admin.home')}}" style="float:right">Home</a></div>
                    <div class="card-body">
                    <table id="formTable" class="table table-bordered table-striped">
                    <thead>
                     @if(isset($submitCount))
                        @foreach($submitCount as $count)
                            @php $formSubmit+=1;@endphp
                        @endforeach
                    @endif
                        <tr>
                            <th>Id</th>
                            <th>Form open</th>
                            <th>Form Submit</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                <tr>
                                    <td>{{1}}</td>
                                    <td>{{$pageView}}</td>
                                    <td>{{$formSubmit}}</td>
                                    <td>
                                        <a title="View User" href="{{url('/admin/userData')}}" class="btn btn-outline-success btn-sm">View User</a>
                                       
                
                                    </td>
                                </tr>
                          
                    </tbody>
                    
                </table>

        </div>

<script type="text/javascript">
    $(function () {
        
        $('#formTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "pageLength": 20
        });
    });
    
</script>
                
            </div>
        </div>
    </div>
</div>
@endsection