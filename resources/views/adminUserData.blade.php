@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard<a href="{{route('admin.home')}}" style="float:right">Home</a></div>
                    <div class="card-body">
                    <table id="formTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>User name</th>
                             <th>Answer</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @if (isset($formData)) 
                         @foreach ($formData as $data)
                                <tr>
                                    <td>{{$userName[$i][0]['name']}}</td>
                                    <td>{{$data->answer}}</td>
                                </tr>    
                                @php $i++; @endphp
                            @endforeach
                        @endif
                    </tbody>
                    
                </table>

        </div>

<script type="text/javascript">
    $(function () {
        
        $('#formTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "pageLength": 20
        });
    });
     
</script>
                
            </div>
        </div>
    </div>
</div>
@endsection