<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return  view('auth.login');
    } 
});
Auth::routes();
Route::group(['prefix'=>'admin','middleware' => ['is_admin']], function () {
Route::get('home', [HomeController::class, 'adminHome'])->name('admin.home');
Route::post('addmore', [HomeController::class, 'addMorePost']);
Route::get('formData', [HomeController::class, 'formData'])->name('admin.formData');
Route::get('userData', [HomeController::class, 'userData'])->name('admin.UserData');
});
Route::group(['prefix'=>'user','middleware' => ['auth']], function () {
Route::get('home', [HomeController::class, 'index'])->name('user.home');
Route::post('addpost', [HomeController::class, 'addPost']);
});